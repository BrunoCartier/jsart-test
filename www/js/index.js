var goodId;

function onReady() {
    console.log('Ready.');

    var video = document.createElement('video');
    document.body.appendChild(video);
    navigator.getUserMedia  = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia || navigator.msGetUserMedia;
    // var tw = 1280 / 2;
    var tw = 400;
    // var th = 720 / 2;
    var th = 400;
    var hdConstraints = {
        audio: false,
        video: {
            deviceId: {
                exact : goodId
            },
            // mandatory: {
            //     maxWidth: tw,
            //     maxHeight: th
            // }
        }
    };
    if (navigator.getUserMedia) {
        navigator.getUserMedia(hdConstraints, success, errorCallback);

    } else {
        errorCallback('');
    }
    function errorCallback(e) {
        console.log("Can't access user media", e);
    }
    function success(stream) {
        console.log('success', stream);
        video.src = window.URL.createObjectURL(stream);
        video.onclick = function() { video.play(); };
        video.play();
        var cameraParam = new ARCameraParam();
        cameraParam.onload = function() {

            var arController;
            var interval = setInterval(function() {
                if (!video.videoWidth)	return;
                if (!arController) {
                    arController = new ARController(video, cameraParam);
                    arController.debugSetup();
                }
                arController.process();
            }, 250);
        };
        cameraParam.src = 'libs/camera_params.dat';
    }
}

// document.addEventListener('deviceready', onReady);
// onReady();



navigator.mediaDevices.enumerateDevices().then(function (devices) {
    console.log('enum resp' + JSON.stringify(devices));
    devices = devices.filter(function(device) {
        console.log('kind', device.kind);
        return device.kind === 'videoinput';
    });

    console.log('devices length' + devices.length);

    if (devices.length > 1) {
        goodId = devices[1].deviceId;
    } else {
        goodId = devices[0].deviceId;
    }

    onReady();
});
